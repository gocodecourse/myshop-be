import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import mongoose from "mongoose";
import {
  getAllProductsController,
  getProductController,
  addProductController,
  addProductsController,
  deleteProductController,
  updateProductController,
} from "./controllers/Products.js";

dotenv.config();
const { PORT, DB_USER, DB_PASS, DB_HOST, DB_NAME } = process.env;

const app = express();
app.use(express.json());
app.use(cors());

mongoose.set("strictQuery", true);

//schemas

const ServiceSchema = new mongoose.Schema({
  productId: {
    type: Number,
    required: true,
  },
  categories: {
    type: Array,
  },
});

// model related to the specific schema
const Services = mongoose.model("Services", ServiceSchema);

app.get("/api/products/getAllProducts", getAllProductsController);
app.get("/api/products/getProduct/:id", getProductController);
app.post("/api/products/addProduct", addProductController);
app.post("/api/products/addProducts", addProductsController);
app.put("/api/products/updateProduct/:id", updateProductController);
app.delete("/api/products/deleteProduct/:id", deleteProductController);

app.get("/api/services/getAllCategories", async (req, res) => {
  try {
    const result = await Services.find({});
    const allCategories = result[0].categories;

    res.status(200).send(allCategories);
  } catch (e) {
    console.log(e);
    res.status(500).send({ message: e });
  }
});

app.get("/api/ping", async (req, res) => {
  try {
    res.status(200).send("pong");
  } catch (e) {
    console.log(e);
    res.status(500).send({ message: e });
  }
});

mongoose.connect(
  `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_NAME}?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (info) => {
    app.listen(PORT, () => {
      console.log("info", info);
      console.log("i am listening");
    });
  }
);
