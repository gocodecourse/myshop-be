import { Products } from "../models/Products.js";

export const getAllProducts = () => {
  return Products.find({});
};

export const getProduct = (id) => {
  return Products.findOne({ _id: id });
};

export const addProduct = (productToAdd) => {
  const newProduct = new Products({
    title: productToAdd.title,
    price: productToAdd.price,
    description: productToAdd.description,
    image: productToAdd.image,
    category: productToAdd.category,
  });

  return newProduct.save();
};

export const addProducts = (productsToAdd) => {
  const newProducts = productsToAdd.map((product) => {
    return new Products({
      title: productsToAdd.title,
      price: productsToAdd.price,
      description: productsToAdd.description,
      image: productsToAdd.image,
      category: productsToAdd.category,
    });
  });

  return Products.insertMany(newProducts);
};

export const deleteProduct = (id) => {
  return Products.findOneAndDelete({ _id: id });
};
