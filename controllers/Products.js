import {
  getAllProducts,
  getProduct,
  addProduct,
  addProducts,
  deleteProduct,
} from "../services/Products.js";
import { keysAllowedUpdates } from "../config/config.js";

export const getAllProductsController = async (req, res) => {
  try {
    const allProducts = await getAllProducts();

    res.status(200).send(allProducts);
  } catch (e) {
    res.status(500).send({ message: e });
  }
};

export const getProductController = async (req, res) => {
  const { id } = req.params;
  try {
    const product = await getProduct(id);
    res.status(200).send(product);
  } catch (e) {
    res.status(500).send({ message: e });
  }
};

export const addProductController = async (req, res) => {
  try {
    const newProduct = await addProduct(req.body);

    res.status(200).send(newProduct);
  } catch (e) {
    console.log(e);
    res.status(500).send({ message: e });
  }
};

export const addProductsController = async (req, res) => {
  try {
    const newProducts = await addProducts(req.body);

    res.status(200).send("newProducts");
  } catch (e) {
    console.log(e);
    res.status(500).send({ message: e });
  }
};

export const deleteProductController = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedProduct = await deleteProduct(id);

    if (!deletedProduct) {
      res.status(404).send({ message: "no such product with the specified id" });
    }
    res.status(200).send(deletedProduct);
  } catch (e) {
    res.status(500).send({ message: e });
  }
};

export const updateProductController = async (req, res) => {
  const { id } = req.params;
  const updates = Object.keys(req.body);

  const isValidOperation = updates.every((update) => keysAllowedUpdates.includes(update));

  if (!isValidOperation) {
    res.status(400).send({ message: "Invalid updates" });
  }

  try {
    const product = await getProduct(id);

    if (!product) {
      res.status(404).send({ message: "product does not exist" });
    }

    updates.forEach((updateKey) => (product[updateKey] = req.body[updateKey]));
    product["updatedAt"] = Date.now();

    await product.save();
    res.status(200).send(product);
  } catch (e) {
    console.log(e);
    res.status(500).send({ message: e });
  }
};
